# Copyright (C) 2001-2016 Quantum ESPRESSO group
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License. See the file `License' in the root directory
# of the present distribution.

include make.inc

default :
	@echo 'to install this , type at the shell prompt:'
	@echo '  ./configure [--prefix=]'
	@echo '  make [-j] target'

###########################################################
# Main targets
###########################################################

# The syntax "( cd PW ; $(MAKE) TLDEPS= all || exit 1)" below
# guarantees that error code 1 is returned in case of error and make stops
# If "|| exit 1" is not present, the error code from make in subdirectories
# is not returned and make goes on even if compilation has failed

miniPWPP : bindir libfftx libcg libla liblapack libutil
	if test -d src ; then \
	( cd src ; $(MAKE) TLDEPS= all || exit 1) ; fi

miniPWPP_k : bindir libfftx libcg libla liblapack libutil
	if test -d src ; then \
	( cd src ; $(MAKE) minipwpp || exit 1) ; fi

miniPWPP_g : bindir libfftx libcg libla liblapack libutil
	if test -d src ; then \
	( cd src ; $(MAKE) minipwpp_gamma || exit 1) ; fi


miniPWPP_ktest : miniPWPP_k
	if test -d examples ; then (cd examples ; ./run_k_examples ) ; fi

miniPWPP_gtest : miniPWPP_g
	if test -d examples ; then (cd examples ; ./run_g_examples ) ; fi

###########################################################
# Auxiliary targets used by main targets:
# compile modules, libraries, directory for binaries, etc
###########################################################

libcg : touch-dummy libla clib libutil
	( cd KSSolverlib/CG ; $(MAKE) TLDEPS= all || exit 1 )

libla : touch-dummy liblapack libutil
	( cd LAXlib ; $(MAKE) TLDEPS= all || exit 1 )

libutil : touch-dummy
	( cd UtilXlib ; $(MAKE) TLDEPS= all || exit 1 )

clib : touch-dummy
	( cd clib ; $(MAKE) TLDEPS= all || exit 1 )

bindir :
	test -d bin || mkdir bin

#############################################################
# Targets for external libraries
############################################################

#libfft : touch-dummy
#	( cd FFTXlib ; $(MAKE) TLDEPS= all || exit 1 )

libfftx : touch-dummy
	cd install ; $(MAKE) -f extlibs_makefile $@


libblas : touch-dummy
	cd install ; $(MAKE) -f extlibs_makefile $@

liblapack: touch-dummy
	cd install ; $(MAKE) -f extlibs_makefile $@


#########################################################
# plugins
#########################################################

touch-dummy :
	$(dummy-variable)

#########################################################
# "make links" produces links to all executables in bin/
#########################################################

# Contains workaround for name conflicts (dos.x and bands.x) with WANT
links : bindir
	( cd bin/ ; \
	rm -f *.x ; \
	for exe in ../*/*/*.x ../*/bin/* ; do \
	    if test ! -L $$exe ; then ln -fs $$exe . ; fi \
	done ; \
	)

#########################################################
# 'make install' works based on --with-prefix
# - If the final directory does not exists it creates it
#########################################################

install : touch-dummy
	@if test -d bin ; then mkdir -p $(PREFIX)/bin ; \
	for x in `find * ! -path "test-suite/*" -name *.x -type f` ; do \
		cp $$x $(PREFIX)/bin/ ; done ; \
	fi
	@echo 'binaries installed in $(PREFIX)/bin'

#########################################################
# Other targets: clean up
#########################################################

# remove object files and executables
clean :
	touch make.inc
	for dir in \
	    src LAXlib UtilXlib KSSolverlib/CG clib \
	; do \
	    if test -d $$dir ; then \
		( cd $$dir ; \
		$(MAKE) TLDEPS= clean ) \
	    fi \
	done
	- @(cd install ; $(MAKE) -f plugins_makefile clean)
#	- @(cd install ; $(MAKE) -f extlibs_makefile clean)
	- /bin/rm -rf bin/*.x tmp

# remove files produced by "configure" as well
veryclean : clean
	- @(cd install ; $(MAKE) -f plugins_makefile veryclean)
	- @(cd install ; $(MAKE) -f extlibs_makefile veryclean)
	- cd install ; rm -f config.log configure.msg config.status \
		ChangeLog* intel.pcl */intel.pcl
	- rm -rf include/configure.h
	- cd install ; rm -fr autom4te.cache
	- cd install; ./clean.sh ; cd -
	- cd include; ./clean.sh ; cd -
	- rm -f project.tar.gz
	- rm -rf make.inc
	- rm -rf bin/
	- @(if test -d examples/results_k; then  rm -rf examples/results_k; fi)
	- @(if test -d examples/results_g; then  rm -rf examples/results_g; fi)

# remove everything not in the original distribution
distclean : veryclean
	( cd install ; $(MAKE) -f plugins_makefile $@ || exit 1 )

tar :
	@if test -f miniPWPP.tar.gz ; then /bin/rm project.tar.gz ; fi
	# do not include unneeded stuff
	find ./ -type f | grep -v -e /.svn/ -e'/\.' -e'\.o$$' -e'\.mod$$'\
		-e /.git/ -e'\.a$$' -e'\.d$$' -e'\.i$$' -e'_tmp\.f90$$' -e'\.x$$' \
		-e'~$$' -e'\./GUI' -e '\./tempdir' | xargs tar rvf miniPWPP.tar
	gzip miniPWPP.tar

#########################################################
# Tools for the developers
#########################################################

depend:
	@echo 'Checking dependencies...'
	- ( if test -x install/makedeps.sh ; then install/makedeps.sh ; fi)
# update file containing version number before looking for dependencies
