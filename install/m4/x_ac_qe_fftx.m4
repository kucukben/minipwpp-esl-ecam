# Copyright (C) 2001-2016 Quantum ESPRESSO Foundation
# Modified for miniPWPP by E. Kucukbenli

AC_DEFUN([X_AC_QE_FFTX], [

# Check for FFT interface libraries 
# Right now the only one I know is FFTXlib...
# So that is the default

fft_interface="FFTXLIB"

  AC_MSG_CHECKING([FFTX])
 
  AC_MSG_RESULT(${fftx_libs})
 
  ]
)
