 subroutine g_psi(npwx, npw, nvec, npol, psi, eig)

! global variables
  USE mpp_module, ONLY : DP
  USE mpp_module, ONLY : ekin
  implicit none
! input variables
  integer, intent(IN) :: npwx, npw, nvec, npol
  complex(DP), intent(INOUT) :: psi(npwx,nvec)
  real(DP), intent(IN) :: eig(nvec)
! local variables
  integer :: ivec, ig
  real(DP) :: x, denm
  
  call start_clock('g_psi')
  do ivec = 1, nvec
     do ig = 1, npw
        x = (ekin(ig) - eig(ivec))
        denm = 0.5_dp*(1.d0+x+sqrt(1.d0+(x-1)*(x-1.d0)))
        psi (ig, ivec) = psi (ig, ivec) / denm
     enddo
  enddo
  call stop_clock('g_psi')

 end subroutine g_psi
