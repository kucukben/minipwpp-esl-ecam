# MiniPWPP


A DFT barebone with planewave basis and crystal pseudopotential, for benchmarking and didactic purposes. 
In this first version a single diagonalization method available (conjugate gradient).


Installation:

```
./configure
make miniPWPP
```


Test installation via:
```
make miniPWPP_ktest
make miniPWPP_gtest
```


First one is for the generic k point, second is to test the gamma-specific routines.


For more details, see doc/readme.rst and examples/README.examples.







